<?php


spl_autoload_register(function ($className) {
	$filePath = str_replace('\\', DIRECTORY_SEPARATOR, __DIR__ . '/students/' .	$className . '.php');
	if (file_exists($filePath)) {

		include $filePath;
	}
});

spl_autoload_register(function ($fullClassName) {
	$filePath = str_replace('\\', DIRECTORY_SEPARATOR, __DIR__ . '/' . 	$fullClassName . '.php');
	if (file_exists($filePath)) {

		include $filePath;
	}
});

spl_autoload_register(function ($className) {
	$filePath = str_replace('\\', DIRECTORY_SEPARATOR, __DIR__ .'\\task\\' .	$className . '.php');
	if (file_exists($filePath)) {
		include $filePath;
	}
});

use \students\Student;
use \students\Teacher;
use \task\Task;

$student_1 = new Student( "Василий",date("d.m.y"));
$student_1->cours = "PHP";
$student_1->group = "4";
$student_1->Show_Info();echo "<br>";
$student_2 = new Student("Петр","","JS","5");
$student_2->Show_Info();echo "<br>";

$teacher_1 = new Teacher("Иван Иванович");
$task_1 = new Task("Задача 1 : 2+2=...",4);
$task_2 = new Task("Задача 2 : 5*2=...",10);
echo $task_2->discription;

$teacher_1->setTaskStudent($student_1,$task_1);
$teacher_1->setTaskStudent($student_2,$task_2);
$student_1->getTask()->result;
$student_1->solve(4);
$student_2->solve(8);
echo("---------------------------------<br>");
$student_1->Show_Info();echo "<br>";
$student_2->Show_Info();echo "<br>";