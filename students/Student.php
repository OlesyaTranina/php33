<?php 
namespace students;

class Student extends Man
{ 
	public $cours = "";
	public $group = "";
	protected $ball = 0;
  protected $task ;
	function __construct($name = "",$birthday ="",$cours = "",$group = ""){
		parent::__construct($name,$birthday);
    $this->cours = $cours;
    $this->group = $group;
	}
	public function Show_Info() {
  	echo	$this->name . " курс " . $this->cours . " группа " . $this->cours . " баллы " . $this->ball;
	}
  public function addBall($ball = 0) {
  	$this->ball += $ball;
  }
  public function getBall() {
  	return $this->ball;
  }
  public function  setTask($task) {
    $this->task = $task;
  }
  public function  getTask() {
    return $this->task;
  }
  public function  solve($result) {
    echo  "-- " . $this->name . " решает задачу " . $this->task ->getName(); echo "<br>";
    $this->addBall($this->task->solve($result));
  }
}
